class CreateTechnologies < ActiveRecord::Migration
  def change
    create_table :technologies do |t|
      t.string :name
      t.string :description
      t.integer :position

      t.references :group, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
