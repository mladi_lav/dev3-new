class AddAttachmentImageToComplexities < ActiveRecord::Migration
  def self.up
    change_table :complexities do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :complexities, :image
  end
end
