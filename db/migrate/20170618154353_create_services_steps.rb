class CreateServicesSteps < ActiveRecord::Migration
  def change
    create_table :services_steps do |t|
      t.belongs_to :service, index: true
      t.belongs_to :step, index: true
    end
  end
end
