class AddAttachmentIconToComplexities < ActiveRecord::Migration
  def self.up
    change_table :complexities do |t|
      t.attachment :icon
    end
  end

  def self.down
    remove_attachment :complexities, :icon
  end
end
