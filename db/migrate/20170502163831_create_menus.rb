class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :name
      t.integer :position
      t.string :link
      t.boolean :published

      t.timestamps null: false
    end
  end
end
