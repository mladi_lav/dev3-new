class CreateEstimates < ActiveRecord::Migration
  def change
    create_table :estimates do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :project
      t.string :budged

      t.timestamps null: false
    end
  end
end
