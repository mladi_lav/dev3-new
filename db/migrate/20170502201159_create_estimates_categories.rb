class CreateEstimatesCategories < ActiveRecord::Migration
  def change
    create_table :categories_estimates, id: false do |t|
      t.belongs_to :estimate, index: true
      t.belongs_to :category, index: true
    end
  end
end
