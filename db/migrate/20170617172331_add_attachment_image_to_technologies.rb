class AddAttachmentImageToTechnologies < ActiveRecord::Migration
  def self.up
    change_table :technologies do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :technologies, :image
  end
end
