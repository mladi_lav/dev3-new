class AddFullTextToService < ActiveRecord::Migration
  def change
    add_column :services, :full_text, :text
  end
end
