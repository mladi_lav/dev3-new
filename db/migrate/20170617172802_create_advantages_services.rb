class CreateAdvantagesServices < ActiveRecord::Migration
  def change
    create_table :advantages_services do |t|
    	t.belongs_to :advantage, index: true
    	t.belongs_to :service, index: true
    end
  end
end
