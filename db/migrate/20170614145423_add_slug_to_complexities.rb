class AddSlugToComplexities < ActiveRecord::Migration
  def change
    add_column :complexities, :slug, :string
    add_index :complexities, :slug, unique: true
  end
end
