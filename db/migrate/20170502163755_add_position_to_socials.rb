class AddPositionToSocials < ActiveRecord::Migration
  def change
    add_column :socials, :position, :integer
  end
end
