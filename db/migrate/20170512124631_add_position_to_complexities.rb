class AddPositionToComplexities < ActiveRecord::Migration
  def change
    add_column :complexities, :position, :integer
  end
end
