class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.text :text
      t.integer :views
      t.integer :likes
      t.string :title
      t.text :description
      t.text :keywords
      t.string :slug
      t.string :behance

      t.timestamps null: false
    end
    add_index :projects, :slug, unique: true
  end
end
