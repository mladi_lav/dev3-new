class CreateComplexitiesEstimates < ActiveRecord::Migration
  def change
    create_table :complexities_estimates do |t|
    	t.belongs_to :estimate, index: true
      	t.belongs_to :complexity, index: true
    end
  end
end
