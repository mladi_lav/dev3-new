class AddAttachmentCaptureToServices < ActiveRecord::Migration
  def self.up
    change_table :services do |t|
      t.attachment :capture
    end
  end

  def self.down
    remove_attachment :services, :capture
  end
end
