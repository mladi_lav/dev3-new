class AddColumnsToComplexities < ActiveRecord::Migration
  def change
    add_column :complexities, :title, :string
    add_column :complexities, :description, :text
    add_column :complexities, :keywords, :text
    add_column :complexities, :stages, :boolean
    add_column :complexities, :form, :boolean
    add_column :complexities, :benefits, :boolean
  end
end
