class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.integer :position
      t.string :title
      t.text :description
      t.text :text
      t.text :short_text
      t.text :keywords
      t.boolean :stages
      t.boolean :form
      t.boolean :benefits
      t.boolean :testimonials
      t.belongs_to :complexity, index: true
      t.string :slug, index: true, unique: true
      
      t.timestamps null: false
    end
  end
end
