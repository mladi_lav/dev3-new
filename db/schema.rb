# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170618154353) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "advantages", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "icon"
    t.integer  "position"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "advantages_services", force: :cascade do |t|
    t.integer "advantage_id"
    t.integer "service_id"
  end

  add_index "advantages_services", ["advantage_id"], name: "index_advantages_services_on_advantage_id", using: :btree
  add_index "advantages_services", ["service_id"], name: "index_advantages_services_on_service_id", using: :btree

  create_table "benefits", force: :cascade do |t|
    t.string   "name"
    t.string   "icon"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "position"
    t.text     "text"
  end

  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true, using: :btree

  create_table "categories_estimates", id: false, force: :cascade do |t|
    t.integer "estimate_id"
    t.integer "category_id"
  end

  add_index "categories_estimates", ["category_id"], name: "index_categories_estimates_on_category_id", using: :btree
  add_index "categories_estimates", ["estimate_id"], name: "index_categories_estimates_on_estimate_id", using: :btree

  create_table "categories_projects", id: false, force: :cascade do |t|
    t.integer "project_id"
    t.integer "category_id"
  end

  add_index "categories_projects", ["category_id"], name: "index_categories_projects_on_category_id", using: :btree
  add_index "categories_projects", ["project_id"], name: "index_categories_projects_on_project_id", using: :btree

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "data_fingerprint"
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["type"], name: "index_ckeditor_assets_on_type", using: :btree

  create_table "complexities", force: :cascade do |t|
    t.string   "name"
    t.text     "text"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "icon_file_name"
    t.string   "icon_content_type"
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.integer  "position"
    t.string   "slug"
    t.string   "title"
    t.text     "description"
    t.text     "keywords"
    t.boolean  "stages"
    t.boolean  "form"
    t.boolean  "benefits"
  end

  add_index "complexities", ["slug"], name: "index_complexities_on_slug", unique: true, using: :btree

  create_table "complexities_estimates", force: :cascade do |t|
    t.integer "estimate_id"
    t.integer "complexity_id"
  end

  add_index "complexities_estimates", ["complexity_id"], name: "index_complexities_estimates_on_complexity_id", using: :btree
  add_index "complexities_estimates", ["estimate_id"], name: "index_complexities_estimates_on_estimate_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "estimates", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "project"
    t.string   "budged"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "menus", force: :cascade do |t|
    t.string   "name"
    t.integer  "position"
    t.string   "link"
    t.boolean  "published"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string   "name"
    t.text     "text"
    t.integer  "views"
    t.string   "title"
    t.text     "description"
    t.text     "keywords"
    t.boolean  "published"
    t.string   "slug"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "posts", ["slug"], name: "index_posts_on_slug", unique: true, using: :btree

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.text     "text"
    t.integer  "views"
    t.integer  "likes"
    t.string   "title"
    t.text     "description"
    t.text     "keywords"
    t.string   "slug"
    t.string   "behance"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "published"
    t.string   "color"
  end

  add_index "projects", ["slug"], name: "index_projects_on_slug", unique: true, using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "name"
    t.integer  "position"
    t.string   "title"
    t.text     "description"
    t.text     "text"
    t.text     "short_text"
    t.text     "keywords"
    t.boolean  "stages"
    t.boolean  "form"
    t.boolean  "benefits"
    t.boolean  "testimonials"
    t.integer  "complexity_id"
    t.string   "slug"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "capture_file_name"
    t.string   "capture_content_type"
    t.integer  "capture_file_size"
    t.datetime "capture_updated_at"
    t.text     "full_text"
  end

  add_index "services", ["complexity_id"], name: "index_services_on_complexity_id", using: :btree
  add_index "services", ["slug"], name: "index_services_on_slug", using: :btree

  create_table "services_steps", force: :cascade do |t|
    t.integer "service_id"
    t.integer "step_id"
  end

  add_index "services_steps", ["service_id"], name: "index_services_steps_on_service_id", using: :btree
  add_index "services_steps", ["step_id"], name: "index_services_steps_on_step_id", using: :btree

  create_table "slides", force: :cascade do |t|
    t.string   "name"
    t.integer  "project_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "position"
  end

  add_index "slides", ["project_id"], name: "index_slides_on_project_id", using: :btree

  create_table "socials", force: :cascade do |t|
    t.string   "name"
    t.string   "icon"
    t.string   "link"
    t.boolean  "published"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "position"
  end

  create_table "stages", force: :cascade do |t|
    t.string   "name"
    t.integer  "position"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "steps", force: :cascade do |t|
    t.string   "name"
    t.text     "text"
    t.integer  "position"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "technologies", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "position"
    t.integer  "group_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "technologies", ["group_id"], name: "index_technologies_on_group_id", using: :btree

  create_table "testimonials", force: :cascade do |t|
    t.string   "name"
    t.text     "text"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "slides", "projects"
  add_foreign_key "technologies", "groups"
end
