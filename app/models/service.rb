# Services
class Service < ActiveRecord::Base
  belongs_to :complexity
  has_and_belongs_to_many :advantages
  has_and_belongs_to_many :steps
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_attached_file :image
  validates_attachment_content_type :image, content_type: %r{\Aimage\/.*\Z}

  attr_accessor :delete_image
  before_validation { image.clear if delete_image == '1' }


  has_attached_file :capture
  validates_attachment_content_type :capture, content_type: %r{\Aimage\/.*\Z}

  attr_accessor :delete_capture
  before_validation { capture.clear if delete_capture == '1' }

  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end
end
