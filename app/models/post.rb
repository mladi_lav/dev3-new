# Blog posts
class Post < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  scope :published, -> { where(published: true) }
  scope :other_posts, -> (post){ where(published: true).where.not(id: post).limit(4) }
  has_attached_file :image,
                    styles: {
                      thumb: '100x100#',
                      small: '1500x1500>',
                      medium: '2000x2000'
                    }

  validates_attachment_content_type :image, content_type: %r{\Aimage\/.*\Z}
  attr_accessor :delete_image
  before_validation { image.clear if delete_image == '1' }
  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end
end
