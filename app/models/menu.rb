# Main menu
class Menu < ActiveRecord::Base
  scope :published, -> { where(published: true) }
end
