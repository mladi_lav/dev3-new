class Step < ActiveRecord::Base
  has_and_belongs_to_many :services
  has_attached_file :image
  validates_attachment_content_type :image, content_type: %r{\Aimage\/.*\Z}
  attr_accessor :delete_image
  before_validation { image.clear if delete_image == '1' }
end
