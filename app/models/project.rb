# Our works
class Project < ActiveRecord::Base
  has_many :slides, -> { order 'position asc' }
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_and_belongs_to_many :categories
  has_attached_file :image,
                    styles: {
                      thumb: '100x100#',
                      small: '1500x1500>',
                      medium: '2000x2000'
                    }

  validates_attachment_content_type :image, content_type: %r{\Aimage\/.*\Z}
  attr_accessor :delete_image
  before_validation { image.clear if delete_image == '1' }

  scope :published, -> { where(published: true) }
  scope :other_projects, -> (project){ where(published: true).where.not(id: project).limit(3) }

  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end
end
