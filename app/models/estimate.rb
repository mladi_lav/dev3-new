# estimate form
class Estimate < ActiveRecord::Base
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :complexities
end
