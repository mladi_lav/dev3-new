# Slides of projects
class Slide < ActiveRecord::Base
  belongs_to :project
  has_attached_file :image,
                    styles: {
                      thumb: '100x100#',
                      small: '1500x1500>',
                      medium: '2000x2000'
                    }

  validates_attachment_content_type :image, content_type: %r{\Aimage\/.*\Z}
  attr_accessor :delete_image
  before_validation { image.clear if delete_image == '1' }
end
