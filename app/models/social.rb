# Social links
class Social < ActiveRecord::Base
  scope :published, -> { where(published: true) }
end
