# category of projects
class Category < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_and_belongs_to_many :projects
  has_and_belongs_to_many :estimates
  has_attached_file :image

  validates_attachment_content_type :image, content_type: %r{\Aimage\/.*\Z}
  attr_accessor :delete_image
  before_validation { image.clear if delete_image == '1' }

  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end
end
