# encoding: utf-8
# frozen_string_literal: true
class CategoriesController < ApplicationController
  before_action :set_category, only: [:show]
  before_action :behance, only: [:show]
  before_action :behance_profile, only: [:show]

  def show
  end

  private

  def set_category
    @category = Category.friendly.find(params[:id])
  end
end
