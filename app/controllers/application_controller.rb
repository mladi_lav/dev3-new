# set menu for pages
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :set_menu

  def set_menu
    @categories = Category.all.order('position ASC')
    @socials = Social.published.order('position ASC')
    @menu = Menu.published.order('position ASC')
  end

  def behance
    options = { access_token: Rails.application.secrets.behance_api_key }
    @behance_client = Behance::Client.new(options)
  end

  def behance_profile
    @behance = @behance_client.user('mladilav20876f')
    @views = @behance['stats']['views']
    @appreciations = @behance['stats']['appreciations']
  end
end
