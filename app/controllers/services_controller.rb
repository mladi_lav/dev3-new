
# encoding: utf-8
# frozen_string_literal: true
class ServicesController < ApplicationController
  before_action :set_service, only: [:show]
  before_action :set_stages, only: [:show]
  before_action :set_benefits, only: [:show]
  before_action :set_testimonials, only: [:show]


  def show
  	@contact = Contact.new unless @service.form == false
  end

  private

  def set_testimonials
    @testimonials = Testimonial.all unless @service.testimonials == false
  end

  def set_benefits
    @benefits = Benefit.all unless @service.benefits == false
  end

  def set_stages
  	@stages = Stage.all unless @service.stages == false
  end

  def set_service
    @service = Service.friendly.find(params[:id])
  end
end
