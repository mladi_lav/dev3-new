# encoding: utf-8
# frozen_string_literal: true
class ContactsController < ApplicationController
  def index
  end

  def new
    @contact = Contact.new
    @google_api_key = Rails.application.secrets.map_api_key 
  end

  def create
    @contact = Contact.new(contact_params)
    @contact.save
    redirect_to contacts_path
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :phone, :message)
  end
end
