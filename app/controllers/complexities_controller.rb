# encoding: utf-8
# frozen_string_literal: true
class ComplexitiesController < ApplicationController
  before_action :set_complexity, only: [:show]
  before_action :set_stages, only: [:show]
  before_action :set_groups, only: [:show, :index]
  before_action :set_benefits, only: [:show]

  def index
    @complexities = Complexity.all.order('position ASC')
    @benefits = Benefit.all
    @contact = Contact.new
  end

  def show
  	@contact = Contact.new unless @complexity.form == false
  end

  private

  def set_benefits
    @benefits = Benefit.all unless @complexity.benefits == false
  end
  def set_groups
    @groups = Group.all
  end
  def set_stages
  	@stages = Stage.all unless @complexity.stages == false
  end
  def set_complexity
    @complexity = Complexity.friendly.find(params[:id])
  end
end
