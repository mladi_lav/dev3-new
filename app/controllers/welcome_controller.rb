# this is main page
class WelcomeController < ApplicationController
  def index
    @projects = Project.published.order('id DESC').limit(5)
    @complexities = Complexity.all.order('position ASC')
    @testimonials = Testimonial.all
  end
end
