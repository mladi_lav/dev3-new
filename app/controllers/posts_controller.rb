# encoding: utf-8
# frozen_string_literal: true
class PostsController < ApplicationController
  before_action :set_post, only: [:show]

  def index
    @posts = Post.published
  end

  def show
    @posts = Post.other_posts(@post.id)
  end

  private

  def set_post
    @post = Post.friendly.find(params[:id])
  end
end
