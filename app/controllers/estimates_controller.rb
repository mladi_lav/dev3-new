# encoding: utf-8
# frozen_string_literal: true
class EstimatesController < ApplicationController
  def index
  end

  def new
    @complexities = Complexity.all
    @estimate = Estimate.new
  end

  def create
    @estimate = Estimate.new(estimate_params)
    save_categories
    save_complexities
    @estimate.save
    redirect_to estimates_path
  end

  private

  def save_categories
    categories = params[:estimate][:categories]
    @estimate.categories << Category.find(categories) unless categories.nil?
  end

  def save_complexities
    complexities = params[:estimate][:complexities]
    @estimate.complexities << Complexity.find(complexities) unless complexities.nil?
  end

  def estimate_params
    params.require(:estimate).permit(:name, :email, :phone, :project, :budged)
  end
end
