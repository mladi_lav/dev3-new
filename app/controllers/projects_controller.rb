# encoding: utf-8
# frozen_string_literal: true
class ProjectsController < ApplicationController
  before_action :behance, only: [:index, :show]
  before_action :behance_profile, only: [:index]
  before_action :set_project, only: [:show]
  before_action :behance_project, only: [:show]

  def index
    @projects = Project.published.order('id DESC')
  end

  def show
    @projects = Project.other_projects(@project.id);
  end

  private

  def behance_project
    unless @project.behance.nil?
      @behance = @behance_client.project(@project.behance)
      @views = @behance['stats']['views']
      @appreciations = @behance['stats']['appreciations']
    end
  end

  def set_project
    @project = Project.friendly.find(params[:id])
  end
end
