# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).on "ready", -> 
  initSwiper()
  initSliderComplexity()
  initInput()
  initOwlCarousel()
  initHandle()

  $('.count').each ->
    $(this).prop('Counter', 0).animate { Counter: $(this).text() },
      duration: 4000
      easing: 'swing'
      step: (now) ->
        $(this).text Math.ceil(now)
        return
    return
  
  $(window).resize ->
    initSliderComplexity()
    return

  $('[data-toogle-menu]').on 'click', (e) ->
    e.preventDefault()
    btn = $(this)
    body = $('body')
    isOpen = body.hasClass('open-menu')
    body[if isOpen then 'removeClass' else 'addClass'] 'open-menu'
    return

  $(document).on 'change', '.form-control', ->
    input = $(this)
    isEmpty = input.val().length == 0
    input[if isEmpty then 'removeClass' else 'addClass'] 'active'
    return

  return


initInput = ->
  $('.form-control').each (index, el) ->
    input = $(el)
    isEmpty = input.val().length == 0
    input[if isEmpty then 'removeClass' else 'addClass'] 'active'
    return
  return

initOwlCarousel = ->
  $('.owl-carousel').each (i, e) ->
    responsive = $(e).data('responsive')
    $(e).owlCarousel
      margin: 30
      dots: true
      responsive: responsive
    return
  return
