// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery_ui
//= require bootstrap
//= require swiper
//= require owl_carousel
//= require welcome


var map;
function initMap() {

        var mapOptions = {
            zoom: 7,
            center: new google.maps.LatLng(49.9907478, 36.2707188), 

            styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
        };

        var mapElement = document.getElementById('map');

        var map = new google.maps.Map(mapElement, mapOptions);

        var places = [
            ['Kharkow', 49.9907478, 36.2707188, 4]
        ];


        for (var i = 0; i < places.length; i++) {
            var place = places[i];
            var marker = new google.maps.Marker({
                position: {lat: place[1], lng: place[2]},
                map: map,
                icon: '/assets/label.png',
                title: place[0],
                zIndex: place[3]
            });
        }
    }
    

function initSwiper(){
    var swiper = new Swiper('.swiper-testimonials', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationBulletRender: function (swiper, index, className) {
            return '<span class="' + className + '">' + (index + 1) + '</span>';
        }
    });
}
var swiperComplexity;

function initSliderComplexity() {

    if($('.swiper-complexity').length == 0) return false;

    if($(window).width() <= 991) {
        swiperComplexity = new Swiper('.swiper-complexity', {
            slidesPerView: 'auto'
        });
    } else {
        if(swiperComplexity != undefined) {
            swiperComplexity.destroy(false, true);
        }
    }

}

function initHandle(){

    if($('#custom-handle').length == 0) return false;

    var handle = $( "#custom-handle .number" );
    var budged = $( "#budged" );

        $( "#slider" ).slider({
            range: "min",
            min: 2000,
            max: 200000,
            value: 2000,
            create: function() {
                handle.text( $( this ).slider( "value" )  + ' $');
                budged.val( $( this ).slider( "value" ));
            },
            slide: function( event, ui ) {
                handle.text( ui.value  + ' $');
                budged.val( $( this ).slider( "value" ));
            }
        });

}
