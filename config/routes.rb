Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  match "/404" => "errors#error404", via: [ :get, :post, :patch, :delete ]

  resources :projects
  resources :categories
  resources :estimates
  resources :contacts
  resources :complexities
  resources :services
  resources :posts
  root 'welcome#index'
end
