require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = 'https://dev-3.com'
SitemapGenerator::Sitemap.create do
  Menu.published.find_each do |menu|
    add menu.link, :lastmod => menu.updated_at
  end

  Project.published.find_each do |project|
    add project_path(project), :lastmod => project.updated_at
  end

  Project.published.find_each do |post|
    add post_path(post), :lastmod => post.updated_at
  end

  Category.find_each do |category|
    add category_path(category), :lastmod => category.updated_at
  end
end
#SitemapGenerator::Sitemap.ping_search_engines # Not needed if you use the rake tasks