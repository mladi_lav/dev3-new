RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar true

  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  config.model Slide do
    nestable_list true
  end


  config.model Step do
    nestable_list true
    navigation_label 'Services'
    navigation_icon 'fa fa-file-text'
  end
  
  config.model Advantage do
    nestable_list true
    navigation_label 'Services'
    navigation_icon 'fa fa-file-text'
  end

  config.model Benefit do
    nestable_list true
    navigation_label 'Services'
    navigation_icon 'fa fa-file-text'
  end

  config.model Group do
    nestable_list true
    navigation_label 'Services'
    navigation_icon 'fa fa-file-text'
  end

  config.model Service do
    nestable_list true
    navigation_label 'Services'
    navigation_icon 'fa fa-file-text'
  end

  config.model Stage do
    nestable_list true
    navigation_label 'Services'
    navigation_icon 'fa fa-file-text'
  end

  config.model Technology do
    nestable_list true
    navigation_label 'Services'
    navigation_icon 'fa fa-html5'
  end

  config.model Post do
    navigation_label 'Blog'
    navigation_icon 'fa fa-file-text'
    edit do
      fields :name, :published, :image, :title, :description, :keywords do 
        help ''
      end
      field :text, :ck_editor
      
    end

  end


  config.model Category do
    navigation_label 'Works'
    navigation_icon 'fa fa-tags'
    
    nestable_list true
    exclude_fields :projects
  end

  config.model Project do
    navigation_label 'Works'
    navigation_icon 'fa fa-briefcase'
  end

  config.model Slide do
    navigation_label 'Works'
    navigation_icon 'fa fa-cubes'
  end

  config.model 'Ckeditor::Asset' do
    visible false
  end

  config.model 'Ckeditor::AttachmentFile' do
    visible false
  end

  config.model 'Ckeditor::Picture' do
    visible false
  end

  config.model Complexity do
    
    nestable_list true
    navigation_label 'Services'
    navigation_icon 'fa fa-html5'
  end

  config.model Testimonial do
    navigation_label 'Main'
    navigation_icon 'fa fa-comment'
  end

  config.model Social do
    navigation_label 'Main'
    nestable_list true
    navigation_icon 'fa fa-bars'
  end

  config.model Menu do
    navigation_label 'Main'
    nestable_list true
    navigation_icon 'fa fa-bars'
  end


  config.model Contact do
    navigation_label 'Messages'
    navigation_icon 'fa fa-envelope'
  end

   config.model Estimate do
    navigation_label 'Messages'
    navigation_icon 'fa fa-envelope'
  end


  config.model User do
    navigation_icon 'fa fa-user icon text-info-lter'
  end
  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
    nestable

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end
